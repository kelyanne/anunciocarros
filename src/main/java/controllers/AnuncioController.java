package controllers;

import entities.Carro;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

/**
 * Created by kelyanne on 11/06/16.
 */
@Controller
public class AnuncioController {

    @RequestMapping(value="/add", method = RequestMethod.GET)
    public ModelAndView save(Carro carro){

        ModelAndView model = new ModelAndView("/anuncio/anuncio-cadastro");
        System.out.println("Cadastrando o carro "+carro);
        return model;
    }
}
